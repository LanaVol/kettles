
# Kettles - shopping website

Website for shopping kettle with different colors according  simple mockup. You can choose what color you like and add to shopping basket. 
## Preview

Link to preview: https://kettles-store.netlify.app/


## Features

- Toggle theme of interface according chosen color of kettle
- Adaptive styles for laptop and mobile screens
- Сomplex navigation menu with accordion elements
- Toggle different kettles from small kettle's buttons and from radio buttons
- adding products to the shopping cart  
- change quality of chosen goods in shopping basket
- Swap-togglment on mobile screen


