const kettles = document.querySelector(".kettles__choose");
const smallKetts = Array.from(document.querySelectorAll(".smallKettle"));
const radio = Array.from(document.querySelectorAll(".lab"));
const main = document.querySelector("main");
const mainImgKettle = document.querySelector(".kettles__main-img");
const videoBtn = document.querySelector(".kettles__video");
const ketMain = document.querySelector(".kettles");

videoBtn.addEventListener("mouseover", changeImgVideoBtn);
videoBtn.addEventListener("mouseleave", changeImgVideoBtn);
ketMain.addEventListener("click", showAction);

function showAction(e) {
  if (e.target.classList.contains("kettles__btn")) {
    addToCart(e);
  } else {
    changeColor(e);
  }
}

// change color and img kettles according the choise
function changeColor(e) {
  main.classList.remove("pink", "red", "beige", "blue");

  smallKetts.forEach((element) => {
    element.classList.remove("white");
  });

  radio.forEach((element) => {
    element.previousElementSibling.checked = false;
  });

  if (e.target.name === "beigeKettle" || e.target.id === "radio4") {
    setKet("beige", 3);
  } else if (e.target.name === "pinkKettle" || e.target.id === "radio3") {
    setKet("pink", 2);
  } else if (e.target.name === "redKettle" || e.target.id === "radio2") {
    setKet("red", 1);
  } else if (e.target.name === "blueKettle" || e.target.id === "radio1") {
    setKet("blue", 0);
  } else {
    // default settings
    setKet("blue", 0);
  }
}
// set main color, image of chosen kettle
function setKet(color, i) {
  main.classList.add(color);
  mainImgKettle.firstElementChild.setAttribute(
    "src",
    `./image/Image_${color}.png`
  );
  smallKetts[i].classList.toggle("white");
  radio[i].previousElementSibling.checked = true;
}

const navItems = document.querySelector(".nav__itemBlock");
const navMenu = document.querySelector(".nav__item");
const productMenu = document.querySelector(".nav__panels-body");
const productMenuTitle = document.querySelectorAll(".nav__panel-title");
const navBurger = document.querySelector(".nav__burger");
const itemPluses = Array.from(document.querySelectorAll(".item-plus"));

const itemRow = document.querySelector(".item-row");

navItems.addEventListener("click", showProductMenu);
productMenu.addEventListener("click", showProductMenu);
navBurger.addEventListener("click", showMenuBurgerOnClick);

// onclick burgerBtn for mobileScreen
function showMenuBurgerOnClick(e) {
  navMenu.classList.toggle("hide");

  if (e.target && !productMenu.classList.contains("hide")) {
    productMenu.classList.add("hide");
  }
  if (
    e.target &&
    navItems.firstElementChild.classList.contains("products-change")
  ) {
    navItems.firstElementChild.classList.toggle("products-change");
    itemRow.classList.toggle("item-row-rotate");
    navItems.firstElementChild.firstElementChild.classList.toggle(
      "products-change-text"
    );
  }
}

function showProductMenu(e) {
  e.preventDefault();

  if (
    e.target === navItems.firstElementChild ||
    e.target === navItems.firstElementChild.firstElementChild
  ) {
    productMenu.classList.toggle("hide");
    itemRow.classList.toggle("item-row-rotate");
    navItems.firstElementChild.classList.toggle("products-change");
    navItems.firstElementChild.firstElementChild.classList.toggle(
      "products-change-text"
    );
    // productMenu.classList.toggle("unhide");
  }

  // show product-menu items in mobile-screen
  Array.from(productMenuTitle).forEach((element, index) => {
    if (e.target !== element) {
      element.nextElementSibling.classList.add("hideMenu");
      itemPluses[index].setAttribute("src", "./image/icon-plus.svg");
    }

    if (e.target === element) {
      element.nextElementSibling.classList.toggle("hideMenu");
      itemPluses[index].setAttribute("src", "./image/icon-minus.svg");
    }
  });
}

function changeImgVideoBtn(e) {
  if (e.type === "mouseover") {
    videoBtn.firstElementChild.setAttribute(
      "src",
      "./image/video-img-white.svg"
    );
  } else if (e.type === "mouseleave") {
    videoBtn.firstElementChild.setAttribute("src", "./image/video-img.svg");
  }
}

// user cart
const products = [
  { name: "blue", price: 150 },
  { name: "red", price: 150 },
  { name: "pink", price: 150 },
  { name: "beige", price: 150 },
];
let userListCards = [];

const basket = document.querySelector(".basketNum");
const basketBtn = document.querySelector(".basket");
const basketInfo = document.querySelector(".basketInfoWrap");
const basketGoods = document.querySelector(".basketGoods");
const closeBasket = document.querySelector(".close");
const totalPrice = document.querySelector(".total");
const body = document.querySelector("body");

basketBtn.addEventListener("click", showCartInfo);
closeBasket.addEventListener("click", closeCartInfo);

// show | hide cart with chosen goods
function showCartInfo() {
  body.classList.add("active");
}

function closeCartInfo(e) {
  e.stopPropagation();
  body.classList.remove("active");
}

// add chosen goods to basket onclick ShopNowBtn
function addToCart(e) {
  products.forEach((product, i) => {
    if (e.target && main.classList.contains(product.name)) {
      if (userListCards[i] == null) {
        userListCards[i] = { ...products[i] };
        userListCards[i].quantity = 1;
      } else {
        userListCards[i].quantity = userListCards[i].quantity + 1;
        userListCards[i].price = products[i].price * userListCards[i].quantity;
      }
    }
  });

  reloadCartInfoGoods();
}

function reloadCartInfoGoods() {
  basketGoods.innerHTML = "";

  let count = 0;
  let totalPr = 0;
  userListCards.forEach((value, i) => {
    totalPr += value.price;
    count += value.quantity;

    if (value !== null) {
      let newDiv = document.createElement("li");
      newDiv.innerHTML = `
      <div><img src='./image/Group_${value.name}.png' class='basketImg'/></div>
       <div>${value.name}</div>
       <div>${value.price} $</div>
       <div>
        <button onclick="changeQuantity(${i}, ${value.quantity - 1})">-</button>
        <div>${value.quantity}</div>
        <button onclick="changeQuantity(${i}, ${value.quantity + 1})">+</button>
      </div>
      `;
      basketGoods.appendChild(newDiv);
    }
  });
  createOrderBtn();
  totalPrice.innerText = `${totalPr} $`;
  basket.innerText = count;
}

function changeQuantity(key, quantity) {
  if (quantity == 0) {
    delete userListCards[key];
  } else {
    userListCards[key].quantity = quantity;
    userListCards[key].price =
      userListCards[key].quantity * products[key].price;
  }
  reloadCartInfoGoods();
  removeOrderBtn();
}

// create | remove orderBtn
function createOrderBtn() {
  const orderBtn = document.createElement("button");
  orderBtn.classList = "order";
  orderBtn.innerText = "Order";
  basketGoods.appendChild(orderBtn);
}

function removeOrderBtn() {
  if (basketGoods.children.length === 1) {
    basketGoods.children[0].remove();
  }
}

function showModalWindowSuccess(message) {
  Swal.fire({
    position: "center",
    showConfirmButton: true,
    icon: "success",
    title: `${message}`,
    showConfirmButton: false,
    timer: 1500,
  });
}

basketGoods.addEventListener("click", (e) => {
  if (e.target.innerText === "Order") {
    showModalWindowSuccess("Success");
    basketGoods.innerHTML = "";
    totalPrice.innerHTML = 0;
    basket.innerHTML = 0;
    userListCards = [];
  }
});

let startX;
let endX;
let mainBg;

main.addEventListener("touchstart", startSwap);
main.addEventListener("touchend", endSwap);

function setKett(color, i) {
  main.classList.add(color);
  mainImgKettle.firstElementChild.setAttribute(
    "src",
    `./image/Image_${color}.png`
  );

  radio[i].previousElementSibling.checked = true;
}

function startSwap(e) {
  startX = e.touches[0].clientX;
  console.log(startX);
}

function endSwap(e) {
  endX = e.changedTouches[0].clientX;
  console.log(endX);
  showMainTheme();
}

function showMainTheme() {
  mainBg = main.classList.value;

  products.forEach((el, i) => {
    if (mainBg === el.name) {
      main.classList.remove("pink", "red", "beige", "blue");
      if (endX > startX) {
        console.log("toRight");

        if (mainBg === "blue") {
          setKett("beige", 3);
        } else {
          setKett(products[i - 1].name, i - 1);
        }
      } else if (endX < startX) {
        console.log("toLeft");
        if (mainBg === "beige") {
          setKett("blue", 0);
        } else {
          setKett(products[i + 1].name, i + 1);
        }
      }
    }
  });
}
